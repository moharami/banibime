import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image, Share, AsyncStorage, Alert, TextInput} from 'react-native';
import FIcon from 'react-native-vector-icons/dist/Feather';
import styles from './styles'
import Axios from 'axios'
export const url = 'http://banibime.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import {connect} from 'react-redux';
import AlertView from '../../components/modalMassage'
import {Actions} from 'react-native-router-flux'

class WalletAcount extends Component {
    constructor(props){
        super(props);
        this.state = {
            code: '',
            loading: false,
            wallet: null,
            requestData: {},
            statuses: {},
            noRequest: false,
            modalVisible: false,
            requestStatus: '',
            register: false
        };
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    shareCode() {
        Share.share(
            {
                title:  this.state.statuses.link + "کد معرفی شما:" ,
                message: 'http://banibime.com/api/v1/invite/'+this.state.statuses.link,
                url: 'http://banibime.com/api/v1/invite/'+this.state.statuses.link,
            },
        )
    }
    addRequest() {
        this.setState({loading: true});
        AsyncStorage.getItem('token').then((info) => {
            const newInfo = JSON.parse(info);
            Axios.post('/request/market/add', {
                user_id: newInfo.user_id
            }).then(response => {
                this.setState({register: true, modalVisible: true, loading: false, requestData: response.data.data});
                Actions.reset('marketing', {openDrawer: this.props.openDrawer});
            })
            .catch((error) =>{
                this.setState({modalVisible: true, loading: false});
            });
        });
    }
    render() {
        const {user} = this.props;
        if(this.state.loading){
            return (<Loader />)
        }
        else return (
            <View style={styles.topContainer}  onPress={() => null}>
                <View style={styles.container}>
                    {
                        this.props.wallet ?
                            <View style={styles.header}>
                                <Text style={styles.headerLabel}>{user.fname} {user.lname}</Text>
                                <Text style={styles.subLabel}>موجودی حساب شما</Text>
                                <View style={styles.price}>
                                    <Text style={styles.priceLabel}>ریال</Text>
                                    <Text style={styles.amount}>{this.props.walletAmount}</Text>
                                </View>
                                <TextInput
                                    keyboardType="numeric"
                                    placeholder="مبلغ شارژ"
                                    placeholderTextColor={'gray'}
                                    underlineColorAndroid='transparent'
                                    value={this.state.text}
                                    style={{
                                        height: 36,
                                        backgroundColor: 'rgb(246, 246, 246)',
                                        paddingRight: 15,
                                        width: '90%',
                                        borderWidth: 1,
                                        borderColor: 'lightgray',
                                        direction: 'rtl',
                                        marginBottom: 10,
                                        marginTop: 15,
                                        borderRadius: 5,
                                    }}
                                    onChangeText={(text) => this.setState({text})}
                                />
                                <TouchableOpacity style={styles.searchButton} onPress={()=> null}>
                                    <Text style={styles.searchText}>شارژ</Text>
                                </TouchableOpacity>
                            </View> :
                            <View style={styles.body}>
                                <Text style={styles.contentLabel}>کد معرفی را به دوستانتان معرفی کنید تادر اولین خریدشان از بانی بیمه اعتبار رایگان دریافت کنند. همچنین شما بعد از خریدشان اعتبار رایگان خرید از بانی بیمه دریافت خواهید کرد</Text>
                                {
                                    this.state.noRequest ?
                                        <View style={{alignItems: 'center', justifyContent: 'center', width: '100%'}}>
                                            <TouchableOpacity onPress={() => this.addRequest()} style={[styles.advertise, {width: '60%'}]}>
                                                <Text style={styles.buttonTitle}>درخواست بازاریابی </Text>
                                            </TouchableOpacity>
                                        </View> : (this.props.requestStatus === 'request'? <Text style={{color: 'red', textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)'}}>درخواست شما در حال بررسی می باشد </Text>:(this.props.requestStatus === 'approve'?
                                            <View style={styles.btnContainer}>
                                                <View style={styles.shareContainer}>
                                                    <TouchableOpacity onPress={() => this.shareCode()} style={styles.shareIcon}>
                                                        <FIcon name="share-2" size={20} color="white" />
                                                    </TouchableOpacity>
                                                    <View style={styles.shareCode}>
                                                        <Text style={styles.codeValue}>{this.state.statuses.link}</Text>
                                                        <Text style={styles.codeLabel}>کد معرفی شما: </Text>
                                                    </View>
                                                </View>
                                            </View> : <Text style={{color: 'red', textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)'}}>درخواست شما قابل تایید نمی باشد</Text>
                                    ))
                                }
                            </View>
                    }
                    <AlertView
                        closeModal={(title) => this.closeModal(title)}
                        modalVisible={this.state.modalVisible}
                        onChange={(visible) => this.setModalVisible(visible)}

                        title={this.state.register ? 'درخواست شما با موفقیت ثبت شد': 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید'}
                    />
                </View>
            </View>
        );
    }
}
function mapStateToProps(state) {
    return {
        user: state.auth.user,

    }
}
export default connect(mapStateToProps)(WalletAcount);