import React, {Component} from 'react';
import {View} from 'react-native';
import {List, SearchBar} from "react-native-elements";

class AppSearchBar extends Component {
    constructor(props){
        super(props);
        this.state = {
            value: ''
        };
    }
    _doSearch(text){
    }
    render() {
        return (
            <View style={{width: '100%'}}>
                <SearchBar
                    containerStyle={{
                        backgroundColor: 'transparent',
                        borderWidth: 0,
                        shadowColor: 'white',
                        borderBottomColor: 'rgb(237, 237, 237)',
                        borderTopColor: 'transparent',
                        borderBottomWidth: 1,

                    }}
                    onChangeText={(text) => this._doSearch(text)}
                    onClearText={() => this.setState({value: ''})}
                    placeholder="جستجو وسیله"
                    inputStyle={{textAlign: 'right',  paddingRight: 20, backgroundColor: 'white'}}
                />
            </View>
        );
    }
}
export default AppSearchBar;