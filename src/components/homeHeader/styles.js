import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        // flexDirection: 'row',
        // alignItems: 'center',
        // justifyContent: 'space-between',
        // backgroundColor: 'white',
        height: 120,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999,
        borderBottomColor: 'lightgray',
        borderBottomWidth: 1,
        paddingRight: 15,
        paddingLeft: 15,
        paddingTop: 20,
        // paddingBottom: 100,

    },
    top: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-end',
        // paddingTop: 30,
        // paddingBottom: 30
    },
    headerTitle: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingRight: '28%'
    },
    name: {
        color: 'gray',
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
    },


    containerS: {
        flex: 1,
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-end',
        transform: [
            {rotateY: '180deg'},
        ],
    },
    navContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'center',
        // width: 84,
        // paddingBottom: 40,

    },
    numContainer: {
        width: 30,
        height: 30,
        alignItems: "center",
        justifyContent: 'center',
        padding: 10,
        backgroundColor: 'red',
        borderRadius: 80
    },
    text: {
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',

    },
    label: {
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingRight: 7
    },
    seprator: {
        paddingRight: 10,
        paddingLeft: 10
    },
});