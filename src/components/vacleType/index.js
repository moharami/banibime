
import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/FontAwesome';

class VacleType extends Component {

    constructor(props){
        super(props);
        this.state = {
            text: 'علی'
        };
    }
    render() {
        return (
            <TouchableOpacity style={[styles.container, {backgroundColor: this.props.active ? 'rgba(51, 197, 117, 1)' : 'white'}]} onPress={() => this.props.closeModal(this.props.item.name)}>
                <Image source={{uri: 'https://cdn.sympli.io/bundles/5b8e7a3350f648101035319b/85f094151e37dd9c320da9f2c56b1dcaad052d34ab/bundle/ios%2F123.png'}} style={styles.image} />
                <Text style={styles.label}>{this.props.item.name}</Text>
            </TouchableOpacity>
        );
    }
}
export default VacleType;