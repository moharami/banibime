import React from 'react';
import {Text, View, TouchableOpacity, ScrollView} from 'react-native';

import styles from './styles'
export default class Steps extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            active: 0
        };
    }
    // componentDidMount() {
    //     this.forceUpdate();
    //     setTimeout(() => {
    //         switch (this.props.active) {
    //             case 3:
    //                 this.myScroll.scrollTo({x: 200, y: 0});
    //                 break;
    //             case 4:
    //                 this.myScroll.scrollTo({x: 350, y: 0});
    //                 break;
    //             case 5:
    //                 this.myScroll.scrollTo({x: 400, y: 0});
    //                 break;
    //             case 6:
    //                 this.myScroll.scrollTo({x: 500, y: 0});
    //                 break;
    //             case 7:
    //                 this.myScroll.scrollTo({x: 600, y: 0});
    //                 break;
    //         }
    //     }, 100)
    // }
    render(){
        return (
            <ScrollView ref={(select) => this.myScroll = select} style={{ transform: [
                { scaleX: -1}

            ],}}
                        horizontal={true} showsHorizontalScrollIndicator={false}
            >
                <View style={styles.container}>
                    <View style={styles.navContainer}>
                        <Text style={[styles.label, {color: this.props.active === 7 ? 'rgba(255, 193, 39, 1)': 'rgba(200, 200, 200, 1)'}]}>پرداخت</Text>
                        <View style={[styles.numContainer, { backgroundColor: this.props.active === 7 ? 'rgba(255, 193, 39, 1)' : 'white' }]}>
                            <Text style={[styles.text, {color: this.props.active === 7 ? 'white': 'rgba(122, 130, 153, 1)'}]}>7</Text>
                        </View>
                    </View>
                    <View style={styles.navContainer}>
                        <Text style={styles.seprator}>/</Text>
                        <Text style={[styles.label, {color: this.props.active === 6 ? 'rgba(255, 193, 39, 1)': 'rgba(200, 200, 200, 1)'}]}>تایید نهایی</Text>
                        <View style={[styles.numContainer, { backgroundColor: this.props.active === 6 ? 'rgba(255, 193, 39, 1)' : 'white' }]}>
                            <Text style={[styles.text, {color: this.props.active === 6 ? 'white': 'rgba(122, 130, 153, 1)'}]}>6</Text>
                        </View>
                    </View>
                    <View style={styles.navContainer}>
                        <Text style={styles.seprator}>/</Text>
                        <Text style={[styles.label, {color: this.props.active === 5 ? 'rgba(255, 193, 39, 1)': 'rgba(200, 200, 200, 1)'}]}>انتخاب زمان</Text>
                        <View style={[styles.numContainer, { backgroundColor: this.props.active === 5 ? 'rgba(255, 193, 39, 1)' : 'white' }]}>
                            <Text style={[styles.text, {color: this.props.active === 5 ? 'white': 'rgba(122, 130, 153, 1)'}]}>5</Text>
                        </View>
                    </View>
                    <View style={styles.navContainer}>
                        <Text style={styles.seprator}>/</Text>
                        <Text style={[styles.label, {color: this.props.active === 4 ? 'rgba(255, 193, 39, 1)': 'rgba(200, 200, 200, 1)'}]}>بارگذاری تصاویر</Text>
                        <View style={[styles.numContainer, { backgroundColor: this.props.active === 4 ? 'rgba(255, 193, 39, 1)' : 'white' }]}>
                            <Text style={[styles.text, {color: this.props.active === 4 ? 'white': 'rgba(122, 130, 153, 1)'}]}>4</Text>
                        </View>
                    </View>
                    <View style={styles.navContainer}>
                        <Text style={styles.seprator}>/</Text>
                        <Text style={[styles.label, {color: this.props.active === 3 ? 'rgba(255, 193, 39, 1)': 'rgba(200, 200, 200, 1)'}]}>مشخصات خریدار</Text>
                        <View style={[styles.numContainer, { backgroundColor: this.props.active === 3 ? 'rgba(255, 193, 39, 1)' : 'white' }]}>
                            <Text style={[styles.text, {color: this.props.active === 3 ? 'white': 'rgba(122, 130, 153, 1)'}]}>3</Text>
                        </View>
                    </View>
                    <View style={styles.navContainer}>
                        <Text style={styles.seprator}>/</Text>
                        <Text style={[styles.label, {color: this.props.active === 2 ? 'rgba(255, 193, 39, 1)': 'rgba(200, 200, 200, 1)'}]}>قیمت ها</Text>
                        <View style={[styles.numContainer, { backgroundColor: this.props.active === 2 ? 'rgba(255, 193, 39, 1)' : 'white' }]}>
                            <Text style={[styles.text, {color: this.props.active === 2 ? 'white': 'rgba(122, 130, 153, 1)'}]}>2</Text>
                        </View>
                    </View>
                    <View style={styles.navContainer}>
                        <Text style={styles.seprator}>/</Text>
                        <Text style={[styles.label, {color: this.props.active === 1 ? 'rgba(255, 193, 39, 1)': 'rgba(200, 200, 200, 1)'}]}>جزییات</Text>
                        <View style={[styles.numContainer, { backgroundColor: this.props.active === 1 ? 'rgba(255, 193, 39, 1)' : 'white' }]}>
                            <Text style={[styles.text, {color: this.props.active === 1 ? 'white': 'rgba(122, 130, 153, 1)'}]}>1</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}