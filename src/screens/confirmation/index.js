import React, {Component} from 'react';
import {ImageBackground, View, TouchableOpacity, Text, TextInput, Image, AsyncStorage, BackHandler, Alert,KeyboardAvoidingView} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/FontAwesome'
import {Actions} from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import Axios from 'axios';
export const url = 'http://banibime.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import {store} from '../../config/store';
import {connect} from 'react-redux';
import AlertView from '../../components/modalMassage'

class Confirmation extends Component {

    secondTextInput=null
    thirdTextInput=null
    fourthTextInput=null
    number=['','','','']

    constructor(props){
        super(props);
        this.state = {
            text: '',
            text1: '',
            text2: '',
            text3: '',
            loading: false,
            codeDetect: false,
            modalVisible: false

        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    confirm() {
        let f = '';
        f = f + this.state.text+ this.state.text1 +this.state.text2 +this.state.text3;
        if(this.state.text === '')
            this.setState({codeDetect: true, modalVisible: true});
        // Alert.alert('','لطفا کد را وارد کنید');
        else{
            this.setState({loading: true});
            Axios.post('/check_code', {
                mobile: this.props.mobile,
                code: f
            }).then(response=> {
                this.setState({
                    loading: false
                }, () => {


                    store.dispatch({type: 'USER_INFO_FETCHED', payload: response.data.data});
                    store.dispatch({type: 'USER_LOGED', payload: true});
                    console.log('confirm data', response);
                    const info = {'token': response.data.access_token, 'mobile': this.props.mobile, 'code': this.state.text, 'expires_at': response.data.expires_at, user_id: response.data.data.id};
                    const newwwwAsync = AsyncStorage.setItem('token', JSON.stringify(info));
                    console.log('newwwwAsync', newwwwAsync);
                    // Actions.home({openDrawer: this.props.openDrawer, loged: true});
                    console.log('profilllllllllle true ? ', this.props.profile);
                    console.log('insbuy true ? ', this.props.insBuy);
                    const info2 = {'loged': true};
                    const newwwwAsync2 = AsyncStorage.setItem('loged', JSON.stringify(info2));
                    console.log('newwwwAsync2', newwwwAsync2);
                    AsyncStorage.getItem('loged').then((info) => {
                        if(info !== null) {
                            const newInfo = JSON.parse(info);
                            console.log('neeeew iiiiinfologed ', newInfo)
                        }
                    })
                    if(this.props.profile) {
                        Actions.profile({openDrawer: this.props.openDrawer, mobile: this.props.mobile})
                    }
                    else if(this.props.insBuy) {

                        let newFactor = this.props.factor;
                        let newUserdetails = this.props.user_details;
                        newUserdetails.user_id = response.data.data.id;
                        newUserdetails._token = response.data.access_token;
                        const id = response.data.data.id;

                        if(this.props.insurType === 'body') {
                            newFactor.user_id = id;
                        }
                        if(this.props.insurType === 'Third') {
                            newFactor.user_id = id;
                        }
                        if(this.props.insurType === 'motor') {
                            newFactor.user_id = id;
                        }
                        else if(this.props.insurType === 'fire') {
                            newFactor.user_id = id;
                        }
                        else if(this.props.insurType === 'complete') {
                            newFactor.user_id = id;
                        }
                        else if(this.props.insurType === 'travel') {
                            newFactor.user_id = id;
                            newFactor.insurance.user_id = id;
                            newFactor.dataSelect.insurance.user_id = id;
                        }
                        else if(this.props.insurType === 'responsible') {
                            newFactor.user_id = id;
                        }
                        else if(this.props.insurType === 'life') {
                            newFactor.user_id = id;
                            newFactor.insurance.user_id = id;
                        }
                        Actions.prices({openDrawer: this.props.openDrawer, insBuy: true, pageTitle:'مشخصات خریدار', factor:newFactor, user_details:newUserdetails, insurType:this.props.insurType, instalment: this.props.instalment})
                    }
                })
            })
            .catch((response) => {
                if(response.response.data.msg === 'ErorrInput' || response.response.data.msg === 'IncorrectCode'){
                    this.setState({
                        loading: false
                    }, () => {
                        // Alert.alert('','لطفا کد صحیح را وارد کنید');
                        this.setState({codeDetect: true, modalVisible: true});

                        // Actions.login({mobile: this.state.text});
                    })
                }
                else {
                    // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    // this.setState({loading: false});
                    this.setState({modalVisible: true, loading: false});

                }
            });
        }
    }
    resend() {
        this.setState({loading: true, text: '', text1: '', text2: '', text3: ''});

        Axios.post('/resend_activation', {
            mobile: this.props.mobile
        }).then(response=> {
            this.setState({
                loading: false
            }, () => {
                console.log('confirm data', response);
            })
        })
        .catch((error) => {
            // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
            // this.setState({loading: false});
            this.setState({modalVisible: true, loading: false});
        });
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    render() {
        const {user} = this.props;
        if(this.state.loading){
            return (<Loader />)
        }
        return (
            <KeyboardAvoidingView style={styles.container}  behavior="padding" enabled>
            {/*<View behavior={'padding'} style={styles.container}>*/}
                {this.state.loading ? <Loader send={false}/> :
                    <View style={styles.send}>
                        <Image style={{alignSelf: 'center', marginRight: 20, width: '40%', resizeMode: 'contain'}} res
                               source={require('../../assets/Logo2.png')}/>
                        <View style={styles.body}>
                            <Text style={styles.header}>ورود به بانی بیمه</Text>
                            <Text style={styles.label}>برای شماره {this.props.mobile} یک کد فرستاده شده کد را وارد کنید</Text>
                            <View style={{
                                display: 'flex',
                                width: '100%',
                                flexDirection: 'row',
                                justifyContent: 'space-around',
                                alignContent: 'space-between'
                            }}>
                                <TextInput
                                    maxLength={1}
                                    keyboardType='numeric'
                                    placeholderTextColor={'#C8C8C8'}
                                    underlineColorAndroid='transparent'
                                    style={{
                                        textAlign: 'center',
                                        borderWidth: 1,
                                        borderColor: '#C8C8C8',
                                        height: 45,
                                        backgroundColor: 'white',
                                        paddingRight: 10,
                                        paddingLeft: 10,
                                        flex: .1,
                                        borderRadius: 7,
                                        fontSize: 18,
                                        color: '#7A8299'
                                    }}
                                    value={this.state.text}
                                    onChangeText={(text) => {
                                        this.setState({text: text})
                                        this.number[0] = text;
                                        this.secondTextInput.focus()
                                    }}/>
                                <TextInput
                                    maxLength={1}
                                    keyboardType='numeric'
                                    placeholderTextColor={'#C8C8C8'}
                                    underlineColorAndroid='transparent'
                                    value={this.state.text1}
                                    ref={(input) => {
                                        this.secondTextInput = input
                                    }}
                                    style={{
                                        textAlign: 'center',
                                        borderWidth: 1,
                                        borderColor: '#C8C8C8',
                                        height: 45,
                                        backgroundColor: 'white',
                                        paddingRight: 10,
                                        paddingLeft: 10,
                                        flex: .1,
                                        borderRadius: 7,
                                        fontSize: 18,
                                        color: '#7A8299'
                                    }}
                                    onChangeText={(text) => {
                                        this.setState({text1: text})
                                        this.number[1] = text;
                                        this.thirdTextInput.focus()
                                    }}/>
                                <TextInput
                                    maxLength={1}
                                    keyboardType='numeric'
                                    placeholderTextColor={'#C8C8C8'}
                                    underlineColorAndroid='transparent'
                                    value={this.state.text2}
                                    ref={(input) => {
                                        this.thirdTextInput = input
                                    }}
                                    style={{
                                        textAlign: 'center',
                                        borderWidth: 1,
                                        borderColor: '#C8C8C8',
                                        height: 45,
                                        backgroundColor: 'white',
                                        paddingRight: 10,
                                        paddingLeft: 10,
                                        flex: .1,
                                        borderRadius: 7,
                                        fontSize: 18,
                                        color: '#7A8299'
                                    }}
                                    onChangeText={(text) => {
                                        this.setState({text2: text})
                                        this.number[2] = text;
                                        this.fourthTextInput.focus()
                                    }}/>
                                <TextInput
                                    maxLength={1}
                                    keyboardType='numeric'
                                    placeholderTextColor={'#C8C8C8'}
                                    underlineColorAndroid='transparent'
                                    value={this.state.text3}
                                    ref={(input) => {
                                        this.fourthTextInput = input
                                    }}
                                    style={{
                                        textAlign: 'center',
                                        borderWidth: 1,
                                        borderColor: '#C8C8C8',
                                        height: 45,
                                        backgroundColor: 'white',
                                        paddingRight: 10,
                                        paddingLeft: 10,
                                        flex: .1,
                                        borderRadius: 7,
                                        fontSize: 18,
                                        color: '#7A8299'
                                    }}
                                    onChangeText={(text) => {
                                        this.setState({text3: text})
                                        this.number[3] = text

                                    }}/>
                            </View>
                            <TouchableOpacity onPress={() => this.confirm()} style={styles.advertise}>
                                <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#3CB1E8', '#3E40DB']} style={styles.advertise}>
                                    <View>
                                        <Text style={styles.buttonTitle}>ورود</Text>
                                    </View>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                        <View  style={styles.footerContainer}>
                            <View style={styles.footer}>
                                <TouchableOpacity onPress={() => this.resend()}>
                                    <Text style={styles.signup}>ارسال مجدد</Text>
                                </TouchableOpacity>
                                <Text style={styles.footerText}>کد جدید دریافت نکردید؟ / </Text>
                            </View>
                            <TouchableOpacity onPress={() => Actions.push('login')}>
                                <Text style={styles.signup}>تغییر شماره</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                }
                <Image style={{height:'20%',zIndex:-1,width:'100%',position:'absolute',bottom:0}} resizeMode={'cover'} source={require('../../assets/company-hero-3.png')}/>
                <AlertView
                    closeModal={(title) => this.closeModal(title)}
                    modalVisible={this.state.modalVisible}
                    onChange={() => this.setState({modalVisible: false})}

                    title={this.state.codeDetect ? 'لطفا کد صحیح را وارد کنید':'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید' }
                />
            {/*</View>*/}
            </KeyboardAvoidingView>
        );
    }
}
export default Confirmation;